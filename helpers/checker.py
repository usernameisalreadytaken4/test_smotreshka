def kangaroo_checker(x):
    return -10000 <= x <= 10000


def event_type_checker(x):
    # ascii
    lowercase = list(range(97, 123))
    digits = list(range(48, 58))
    return ord(x) in lowercase, ord(x) in digits
