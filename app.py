import datetime
import os

from flask import Flask, request, abort
from flask_pymongo import PyMongo
from helpers.checker import event_type_checker

STARTED_EVENT = 0
FINISHED_EVENT = 1
NOW = datetime.datetime.now

app = Flask(__name__)
app.config["MONGO_URI"] = f"mongodb://{os.environ.get('MONGODB_HOSTNAME')}:27017/{os.environ.get('MONGODB_DATABASE')}"
events = PyMongo(app)


@app.route("/v1/start", methods=["POST"])
def start_event():
    event_type = request.json.get("type")

    if not event_type:
        abort(403, "Need 'type'")
    if not all(map(event_type_checker, event_type)):
        abort(403, "only lower case and digits")

    post = events.db.posts.find_one({"type": event_type})

    if post:
        return "", 200

    events.db.posts.insert_one({
        "type": event_type,
        "state": STARTED_EVENT,
        "started_at": NOW(),
    })

    return "", 200


@app.route("/v1/finish", methods=["POST"])
def end_event():
    event_type = request.json.get("type")
    if not event_type:
        abort(403, "Need 'type'")
    if not all(map(event_type_checker, event_type)):
        abort(403, "only lower case and digits")

    post = events.db.posts.find_one(
        {
            "type": event_type,
            "state": STARTED_EVENT
        }
    )
    if not post:
        abort(404, "event not found")

    new_data = {
        "$set":
            {
                "state": FINISHED_EVENT,
                "finished_at": NOW()
            }
    }

    events.db.posts.update_one({"_id": post["_id"]}, new_data)

    return "", 200


if __name__ == '__main__':
    app.run(debug=True, port=os.environ.get('PORT', 5000), host='0.0.0.0')
