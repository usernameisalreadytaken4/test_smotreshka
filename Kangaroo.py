from helpers.checker import kangaroo_checker


def kangaroo(x1, v1, x2, v2):
    """
    :param x1: Стартовое положение кенгуру #1
    :param v1: Дальность прыжка кенгуру #1
    :param x2: Стартовое положение кенгуру #2
    :param v2: Дальность прыжка кенгуру #2
    :return: bool встретятся кенгуру, или нет.
    """

    distance = abs(x2 - x1)

    while True:
        x1 += v1
        x2 += v2
        if abs(x1 - x2) >= distance:
            return False
        elif abs(x1 - x2) == 0:
            return True
        distance = abs(x1 - x2)


if __name__ == '__main__':
    args = input()
    args = list(map(int, args.split()))
    if not all(list(map(kangaroo_checker, args))):
        raise ValueError("input from 10000 to 10000")
    result = kangaroo(*args)
    print("YES" if result else "NO")
