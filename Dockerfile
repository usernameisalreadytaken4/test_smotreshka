FROM python:3.6

# Copy requirements files
COPY requirements.txt /tmp/

# Install requirement packages
RUN pip install --upgrade pip && pip install -r /tmp/requirements.txt

ADD . /test_smotreshka

# Set workdir
WORKDIR '/test_smotreshka'
